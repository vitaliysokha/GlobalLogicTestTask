﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestTaskApp.Models
{
    [Serializable]
   public class FolderStructure
    {
        private List<string> _directories;
        private List<string> _files;
        private readonly string _sourcePath;

        public FolderStructure()
        {
            _directories = new List<string>();
            _files = new List<string>();
            _sourcePath = "";
        }

        public FolderStructure(string[] dirs, string[] files)
        {
            _directories = new List<string>();
            _files = new List<string>();

            foreach (string dir in dirs)
            {
                _directories.Add(dir);
            }

            foreach (string file in files)
            {
                _files.Add(file);
            }
            if (_directories.Count > 0)
            {
                _sourcePath = Directory.GetParent(_directories.First()).FullName;
            }
            else if (_files.Count > 0)
            {
                _sourcePath = new DirectoryInfo(_files.First()).FullName;
            }
        }

        public void Unpack(string directoryPath)
        {
            foreach (string el in Directory.GetDirectories(_sourcePath, "*",
                SearchOption.AllDirectories))
                Directory.CreateDirectory(el.Replace(_sourcePath, directoryPath));

            foreach (string newPath in Directory.GetFiles(_sourcePath, "*.*",
                SearchOption.AllDirectories))
                 File.Copy(newPath, newPath.Replace(_sourcePath, directoryPath), true);

        }

    }
}
