﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using System.Windows.Input;
using TestTaskApp.Annotations;
using TestTaskApp.Commands;
using TestTaskApp.Models;
using TestTaskApp.Views.Services;


namespace TestTaskApp.ViewModels
{
  public  class MainViewModel: INotifyPropertyChanged
    {
        private string _serializeInput;
        private string _deserializeInput;
        private string _deserializeOutput;
        private readonly IMessageService _messageService;

        public MainViewModel(IMessageService messageService)
        {
            _messageService = messageService;
        }

        #region Properties
        public string SerializeInput
        {
            get { return _serializeInput; }
            set
            {
                _serializeInput = value;
                OnPropertyChanged();
            }
        }

        public string DeserializeInput
        {
            get { return _deserializeInput; }
            set
            {
                _deserializeInput = value;
                OnPropertyChanged();
            }
        }

        public string DeserializeOutput
        {
            get { return _deserializeOutput; }
            set
            {
                _deserializeOutput = value;
                OnPropertyChanged();
            }
        }
        #endregion

        #region Commands
        public ICommand SelectInputFolderCommand
        {
            get
            {
                return new RelayCommand(p => SelectInputFolder());
            }
        }
        public ICommand SerializeCommand
        {
            get
            {
                return new RelayCommand(p => Serialize());
            }
        }

        public ICommand SelectBinFileCommand
        {
            get
            {
                return new RelayCommand(p => SelectBinFile());
            }
        }

        public ICommand SelectOutputFolderCommand
        {
            get
            {
                return new RelayCommand(p => SelectOutputFolder());
            }
        }

        public ICommand DeserializeCommand
        {
            get
            {
                return new RelayCommand(p => Deserialize());
            }
        }

        #endregion


        #region Methods
        private void SelectInputFolder()
        {

            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();

            if (!string.IsNullOrWhiteSpace(fbd.SelectedPath))
                SerializeInput = fbd.SelectedPath;
        }

        public void Serialize()
        {
            string inputPath = SerializeInput;
            string outputPath = "folder.bin";

            var dirs = Directory.GetDirectories(inputPath, "*", SearchOption.AllDirectories).Select(Path.GetFullPath).ToArray();
            var files = Directory.GetFiles(inputPath, "*", SearchOption.AllDirectories).Select(Path.GetFullPath).ToArray();

            FolderStructure folderStructure = new FolderStructure(dirs, files);

            using (Stream stream = File.Open(outputPath, FileMode.Create))
            {
                var binaryFormatter = new BinaryFormatter();
                try
                {
                    binaryFormatter.Serialize(stream, folderStructure);
                    _messageService.ShowMessage($"Success! Result binary file is:\n{new FileInfo(outputPath).FullName} \n");

                }
                catch (Exception ex)
                {
                    _messageService.ShowError(ex.Message);
                }
            }
        }

        public void SelectBinFile()
        {
            OpenFileDialog opd = new OpenFileDialog();
            opd.Filter = "(*.bin)|*.bin";

            DialogResult result = opd.ShowDialog();

            if (!string.IsNullOrWhiteSpace(opd.FileName))
                DeserializeInput = opd.FileName;
        }

        public void SelectOutputFolder()
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            DialogResult result = fbd.ShowDialog();

            if (!string.IsNullOrWhiteSpace(fbd.SelectedPath))
                DeserializeOutput = fbd.SelectedPath;
        }

        public void Deserialize()
        {
            using (FileStream stream = File.Open(DeserializeInput, FileMode.Open))
            {
                var binaryFormatter = new BinaryFormatter();

                try
                {
                    FolderStructure sourceFolder = (FolderStructure)binaryFormatter.Deserialize(stream);
                    sourceFolder.Unpack(DeserializeOutput);
                    _messageService.ShowMessage("Folder was successfully unpacked");
                }
                catch (Exception ex)
                {
                    _messageService.ShowError(ex.Message);
                }
            }
        }

        #endregion



        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {

            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
